package com.hcl.gradedassignment2;

import java.util.ArrayList;

import java.util.TreeMap;

public class DataStructureB {
	
	public void cityNameCount(ArrayList<Employee>employees) {
		TreeMap<String,Integer>countcity = new TreeMap<>();
		for(Employee emp : employees){
			String city = emp.getCity();
			if(countcity.containsKey(city)) {
				countcity.replace(city, (((int)countcity.get(city))+1));
			}
			else
			{
				countcity.put(city, 1);
			}
		}
		System.out.println();
		System.out.println("count of employees from each city");
		System.out.println(" " + countcity);

  }

}
