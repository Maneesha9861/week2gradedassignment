package com.hcl.gradedassignment2;

import java.util.ArrayList;

public class MainClass {

	public static void main(String[] args) {
		
	try
	{
		Employee employee1 = new Employee();
		Employee employee2 = new Employee();
		Employee employee3 = new Employee();
		Employee employee4 = new Employee();
		Employee employee5 = new Employee();
		
		
		employee1.setId(1);
		employee1.setName("Aman");
		employee1.setAge(20);
		employee1.setSalary(1100000);
		employee1.setDepartment("IT");
		employee1.setCity("Delhi");
		
		
		employee2.setId(2);
		employee2.setName("Bobby");
		employee2.setAge(22);
		employee2.setSalary(500000);
		employee2.setDepartment("HR");
		employee2.setCity("Bombay");
				
				
		employee3.setId(3);
		employee3.setName("Zoe");
		employee3.setAge(20);
		employee3.setSalary(750000);
		employee3.setDepartment("Admin");
		employee3.setCity("Delhi");
				
			
		employee4.setId(4);
		employee4.setName("Smitha");
		employee4.setAge(21);
		employee4.setSalary(100000);
		employee4.setDepartment("IT");
		employee4.setCity("Chennai");
				
				
		employee5.setId(5);
		employee5.setName("Smitha");
		employee5.setAge(24);
		employee5.setSalary(1200000);
		employee5.setDepartment("HR");
		employee5.setCity("Bangalore");
				
				ArrayList<Employee> emp = new ArrayList<Employee>();
				emp.add(employee1);
				emp.add(employee2);
				emp.add(employee3);
				emp.add(employee4);
				emp.add(employee5);
				
			
			
				for(Employee em : emp) {
					if(em.getId()<0||em.getName()==null||em.getName().isEmpty()||em.getAge()<0||em.getSalary()<0||em.getDepartment()==null||em.getDepartment().isEmpty()||em.getCity()==null||em.getCity().isEmpty()) {
						throw new IllegalArgumentException();
					}
				}

				System.out.println("List of Employees : ");
				System.out.println("Id     Name      Age      Salary(INR)    Department   Location");
				for (Employee em : emp) {
					System.out.println(em.getId() + "      " + em.getName() + "     " + em.getAge() + "      "
							+ em.getSalary() + "          " + em.getDepartment() + "          " + em.getCity());
				}
				System.out.println();
				DataStructureA ob1 = new DataStructureA();
				ob1.sortingNames(emp);
				DataStructureB ob2 = new DataStructureB();
				ob2.cityNameCount(emp);
				Monthlysalarymethod ob3 = new Monthlysalarymethod();
				ob3.monthlySalary(emp);
				

				}
	
				catch(IllegalArgumentException ref1) 
				{
					System.out.println(ref1.getMessage());
				
				}

}
}



		
			




