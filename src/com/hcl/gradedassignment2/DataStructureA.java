package com.hcl.gradedassignment2;

import java.util.ArrayList;
import java.util.Collections;

public class DataStructureA {
	
	public void sortingNames(ArrayList<Employee>employees) {
		ArrayList<String> list = new ArrayList<String>();
		for(Employee e : employees) {
			list.add(e.getName());
		}
		Collections.sort(list);
		System.out.println();
		System.out.println("Name of all the employees in sorted order are");
		System.out.println(list);
		
		}


}
