package com.hcl.gradedassignment2;

public class Employee {
	
	private int id;
	private String name;
	private int age; 
	int salary;
	private String department;
	private String city;
	
	
	public void setId(int id) {
		if(id<0) {
			throw new IllegalArgumentException("id is not valid");
		}
		this.id = id;
	}
	public void setName(String name) {
		if(name == null || name.isEmpty()) {
			throw new IllegalArgumentException("name should not be empty");
				}
		this.name = name;
	}
	public void setAge(int age) {
		if(age<0) {
			throw new IllegalArgumentException("age cannot be 0");
	}
		this.age = age;
	}
	public void setSalary(int salary) {
		if(salary<0) {
			throw new IllegalArgumentException("salary cannot be zero");
		}
		this.salary = salary;
	}
	public void setDepartment(String department) {
		if(department==null || department.isEmpty()) {
			throw new IllegalArgumentException("department name should not be empty");
		}
		this.department = department;
	}
	public void setCity(String city) {
		if(city==null || city.isEmpty() ) {
			throw new IllegalArgumentException("city name should not be empty");
		}
		this.city = city;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public int getSalary() {
		return salary;
	}
	public String getDepartment() {
		return department;
	}
	public String getCity() {
		return city;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", department="
				+ department + ", city=" + city + "]";
	}
	

}
